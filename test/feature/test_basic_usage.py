import pytest

from pyroman.core.backend.base import InMemoryBackend, BackendRegistry
from pyroman.core.dataset      import Dataset
from pyroman.core.relation     import Relation
from pyroman.core.repository   import Repository
from pyroman.core.schema       import fields

class Users(Relation):
  class SchemaDefinition:
    first_name = fields.CharField()
    last_name  = fields.CharField()

class TestRepository(Repository):
  users = Users


@pytest.fixture
def repository_factory():
  return TestRepository.in_memory

@pytest.fixture
def repository(repository_factory):
  return repository_factory()

@pytest.fixture
def initialize_data(repository):
  repository.users.create(Users.c.first_name << 'Mehmed', Users.c.last_name << 'PyROMan')
  repository.users.create(Users.c.first_name << 'Guido',  Users.c.last_name << 'van Rossum')


def test_repository_setup(repository):
  assert isinstance(repository.users, Users)

def test_initialize_repository_with_data(repository_factory):
  data       = { 'users': [{ 'first_name': 'Mehmed', 'last_name': 'PyROMan' }] }
  repository = repository_factory(data = data)

  assert list(repository.users.all()) == [{ 'first_name': 'Mehmed', 'last_name': 'PyROMan' }]

class RecordFilteringTests:
  @pytest.mark.skip("not implemented feature")
  @pytest.mark.usefixtures('initialize_data')
  def test_where_accepts_args(self, repository):
    users = repository.users.where(Users.c.first_name == 'Mehmed')

    assert list(users) == [{ 'first_name': 'Mehmed', 'last_name': 'PyROMan' }]

  @pytest.mark.usefixtures('initialize_data')
  def test_where_accepts_kwargs(self, repository):
    users = repository.users.where(first_name = 'Mehmed')

    assert list(users) == [{ 'first_name': 'Mehmed', 'last_name': 'PyROMan' }]

  def test_where_without_filters_specified(self, repository):
    users = repository.users.where()

    assert list(users) == list(repository.users.all())

class CreateRecordTests:
  def test_using_kwargs_only(self, repository):
    user = repository.users.create(first_name = 'Mehmed', last_name = 'PyROMan')

    expected = { 'first_name': 'Mehmed', 'last_name': 'PyROMan' }

    assert user                         == expected
    assert list(repository.users.all()) == [expected]


  def test_using_dsl(self, repository):
    user = repository.users.create(Users.c.first_name << 'Mehmed',
                                   Users.c.last_name  << 'PyROMan')

    expected = { 'first_name': 'Mehmed', 'last_name': 'PyROMan' }

    assert user                         == expected
    assert list(repository.users.all()) == [expected]

  def test_using_dsl_and_kwargs(self, repository):
    user = repository.users.create(Users.c.first_name << 'Mehmed',
                                   Users.c.last_name  << 'The Pythonista',
                                   last_name = 'PyROMan')

    expected = { 'first_name': 'Mehmed', 'last_name': 'PyROMan' }

    assert user                         == expected
    assert list(repository.users.all()) == [expected]

def test_register_backend():
  registry = BackendRegistry()

  with pytest.raises(KeyError):
    registry['in_memory']

  registry.register(in_memory = InMemoryBackend)

  assert registry['in_memory'] is InMemoryBackend

class RelationTests:
  def test_all_returns(self, repository):
    assert isinstance(repository.users.all(), Users.__dataset_class__)

  def test_where_returns(self, repository):
    assert isinstance(repository.users.where(), Users.__dataset_class__)

@pytest.mark.skip
def test_backend_adapter_lookup():
  tasks_repo = TestRepository.yaml(filename = '')

# TestRepository(connection, only = ['tasks'])

## feature: hide specific relations for repository (`only`/`exclude`)
# TaskRepository: RepositoryBuilder = TestRepository.only(TestRepository.tasks),
# alternative can be inheritance of repository and removing relation by:
# class TaskRepository(TestRepository):
#     users = None
@pytest.mark.skip("not implemented feature")
def test_repository_builder():
  repository = TestRepository.only(TestRepository.tasks).postgres(username = 'pyroman')

  assert isinstance(repository, TestRepository)

  with pytest.raises():
    assert repository.users
