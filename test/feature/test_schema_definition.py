import pytest
from pyroman.core.schema   import Schema, fields
from pyroman.core.relation import Relation, ImproperlyConfigured, MultipleSchemaDefined

class SchemaDefinitionTests:
  def test_basic_schema_definition(self):
    class Users(Relation):
      class SchemaDefinition:
        first_name = fields.CharField()
        last_name  = fields.CharField()

    assert not hasattr(Users, 'SchemaDefinition')
    assert     hasattr(Users, '__schema__')

    assert issubclass(Users.__schema__, Schema)

  def test_advanced_schema_definition(self):
    class UserSchema(Schema):
      first_name = fields.CharField()
      last_name  = fields.CharField()

    class Users(Relation):
      __schema__ = UserSchema

    assert not hasattr(Users, 'SchemaDefinition')
    assert     hasattr(Users, '__schema__')

    assert Users.__schema__ is UserSchema

  def test_relation_without_schema_definition(self):
    with pytest.raises(ImproperlyConfigured, match = r'(__schema__)*(SchemaDefinition)'):
      class Users(Relation): pass

  def test_relation_with_multiple_schema_definition(self):
    class UserSchema(Schema):
      first_name = fields.CharField()
      last_name  = fields.CharField()

    with pytest.raises(MultipleSchemaDefined):
      class Users(Relation):
        __schema__ = UserSchema

        class SchemaDefinition:
          first_name = fields.CharField()
          last_name  = fields.CharField()
