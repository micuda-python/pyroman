import attr
from copy import copy

from pyroman.core.columns import Column
from pyroman.core.dataset import Dataset
from pyroman.core.schema  import Schema

class MultipleSchemaDefined(Exception): pass
class ImproperlyConfigured(TypeError):  pass


def create_columns_definition(name, column_names):
  return attr.make_class(name, attrs = column_names, frozen = True)


class RelationType(type):
  def __new__(mcls, name, bases, attrs):
    super_new = super().__new__

    # Also ensure initialization is only performed for subclasses of Relation
    # (excluding Relation class itself).
    parents = [b for b in bases if isinstance(b, mcls)]
    if not parents:
      return super_new(mcls, name, bases, attrs)

    schema_definition = mcls._get_schema_class(
      relation_name     = name,
      schema            = attrs.pop('__schema__',       None),
      schema_definition = attrs.pop('SchemaDefinition', None)
    )

    columns = { field_name: Column(field)
                for field_name, field
                in  schema_definition.fields.as_dict().items() }
    columns_definition = create_columns_definition(name         = f'{name}Columns',
                                                   column_names = list(columns))

    new_attrs = { '__dataset_class__': type(f'{name}Dataset', (Dataset, ), {}),
                  '__schema__':        schema_definition,
                  'c':                 columns_definition(**columns),
                  **attrs }

    return super_new(mcls, name, bases, new_attrs)

  @staticmethod
  def _get_schema_class(*, relation_name, schema, schema_definition):
    if schema and schema_definition:
      raise MultipleSchemaDefined(
        "Schema definition is ambiguous. "
        "Please declare only the '__schema__' or 'SchemaDefinition' attribute."
      )

    if schema and issubclass(schema, Schema):
      return schema

    elif schema_definition:
      return Schema.from_schema_definition(class_name = f'{relation_name}Schema',
                                           attrs      = vars(schema_definition).items())

    else:
      raise ImproperlyConfigured("'__schema__' or 'SchemaDefinition' must be provided.")

class Relation(metaclass = RelationType):
  def __init__(self, *, schema_provider, data_provider, declared_as):
    self._schema_provider = schema_provider
    self._data_provider   = data_provider
    self._declared_as     = declared_as

  def create_dataset(self, **kwargs):
    return self.__dataset_class__(data_key      = self._declared_as,
                                  data_provider = self._data_provider,
                                  **kwargs)

  def create(self, *expressions, **kwargs):
    return self._data_provider.insert(
      data_key    = self._declared_as,
      record_data = { **self._expressions_to_dict(expressions), **kwargs }
    )

  def _expressions_to_dict(self, expressions):
    return dict([exp.evaluate(self._schema_provider) for exp in expressions])

  def all(self):
    return self.create_dataset()

  def where(self, **kwargs):
    return self.create_dataset(filters = kwargs)
