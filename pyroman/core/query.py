class Query:
  def __init__(self, filters = None):
    self._filters = {} if filters is None else filters

  filters = property(lambda self: self._filters.copy())
