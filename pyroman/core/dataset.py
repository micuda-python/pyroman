from pyroman.core.query import Query

class Dataset:
  def __init__(self, *, data_key, data_provider, **query_options):
    self._data_key      = data_key
    self._data_provider = data_provider
    self._query         = Query(**query_options)

  def __iter__(self):
    return iter(self._data_provider.fetch(data_key = self._data_key, query = self._query))
