import attr
from typing              import ItemsView
from pyroman.core.schema import Field

__all__ = [
  'SchemaType',
  'Schema'
]

def pick_fields(iterable: ItemsView):
  return {
    field_name: field
    for field_name, field in iterable if isinstance(field, Field)
  }


class BaseFieldDefinition:
  def as_dict(self): return attr.asdict(self)

def create_fields_definition(name, field_names):
  return attr.make_class(name, attrs = field_names, bases = (BaseFieldDefinition, ), frozen = True)


class SchemaType(type):
  def __new__(mcls, name, bases, attrs):
    super_new = super().__new__

    # Also ensure initialization is only performed for subclasses of Schema
    # (excluding Schema class itself).
    parents = [b for b in bases if isinstance(b, mcls)]
    if not parents:
      return super_new(mcls, name, bases, attrs)

    fields            = pick_fields(attrs.items())
    fields_definition = create_fields_definition(name        = f'{name}Fields',
                                                 field_names = list(fields))

    new_attrs = { 'fields': fields_definition(**fields) }

    return super_new(mcls, name, bases, new_attrs)

class Schema(metaclass = SchemaType):
  @classmethod
  def from_schema_definition(cls, *, class_name: str, attrs: ItemsView):
    return type(class_name, (cls, ), pick_fields(attrs))
