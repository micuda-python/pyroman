from abc    import ABC, abstractmethod
from pydash import py_

class Backend(ABC):
  # schema
  column_name = abstractmethod(lambda self, *, field: None)

  # data
  insert    = abstractmethod(lambda self, *, data_key, record_data: None)
  fetch_all = abstractmethod(lambda self, *, data_key:              None)

class InMemoryBackend(Backend):
  def __init__(self, *, metadata, data = None):
    self._metadata = metadata
    self._data     = self._valid_data(data              = data or {},
                                      defined_data_keys = self._metadata['relation_names'])

  @staticmethod
  def _valid_data(data, defined_data_keys):
    missing_keys = set(defined_data_keys) - set(data.keys())
    only_defined = py_.pick(data, *defined_data_keys)

    defaults = { data_key: [] for data_key in missing_keys }

    return py_.assign({}, defaults, only_defined)

  def column_name(self, *, field):
    return field.field_name

  def insert(self, *, data_key, record_data):
    self._data[data_key].append(record_data)

    return record_data

  def fetch(self, *, data_key, query):
    if query.filters: return self.fetch_filtered(data_key = data_key, filters = query.filters)
    else:             return self.fetch_all(data_key = data_key)

  def fetch_all(self, *, data_key):
    return self._data[data_key]

  def fetch_filtered(self, *, data_key, filters):
    return py_.filter_(self.fetch_all(data_key = data_key), filters)

class BackendRegistry:
  def __init__(self):           self._registry = {}
  def __iter__(self):           return iter(self._registry.items())
  def __repr__(self):           return f'{self.__class__.__name__}({self._registry!r})'
  def __getitem__(self, key):   return self._registry[key]

  def register(self, **kwargs): self._registry.update(**kwargs)

default_registry = BackendRegistry()
default_registry.register(in_memory = InMemoryBackend)
