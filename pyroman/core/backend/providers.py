class ProviderBase:
  def __init__(self, backend):
    self._backend = backend

class SchemaProvider(ProviderBase):
  def column_name(self, **kwargs):
    return self._backend.column_name(**kwargs)

class DataProvider(ProviderBase):
  def insert(self, **kwargs):
    return self._backend.insert(**kwargs)

  def fetch(self, **kwargs):
    return self._backend.fetch(**kwargs)
