from functools import partial

from pyroman.core                   import backend
from pyroman.core.backend.providers import SchemaProvider, DataProvider
from pyroman.core.relation          import RelationType

class RepositoryType(type):
  def __new__(mcls, name, bases, attrs):
    super_new = super().__new__

    # Also ensure initialization is only performed for subclasses of Repository
    # (excluding Repository class itself).
    parents = [b for b in bases if isinstance(b, mcls)]
    if not parents:
      return super_new(mcls, name, bases, attrs)

    relations = { attr_name: relation_class
                  for attr_name, relation_class
                  in  attrs.items()
                  if  isinstance(relation_class, RelationType) }

    repository_metadata = { 'relation_names': relations.keys() }
    custom_constructors = mcls._create_constructors_for_backends(repository_metadata)

    new_attrs = { '__module__':    attrs.pop('__module__'),
                  '__relations__': relations,
                  **custom_constructors }

    if (classcell := attrs.pop('__classcell__', None)) is not None:
      new_attrs['__classcell__'] = classcell

    return super_new(mcls, name, bases, new_attrs)

  @staticmethod
  def _create_constructors_for_backends(repository_metadata):
    def constructor_factory(backend_class, repo_class, **kwargs):
      return repo_class(backend_class(metadata = repository_metadata, **kwargs))

    return { backend_alias: classmethod(partial(constructor_factory, backend_class))
             for backend_alias, backend_class
             in  backend.default_registry }

class Repository(metaclass = RepositoryType):
  def __new__(cls, backend):
    self = super().__new__(cls)

    data_provider   = DataProvider(backend = backend)
    schema_provider = SchemaProvider(backend = backend)

    for attr_name, relation_class in cls.__relations__.items():
        relation = relation_class(schema_provider = schema_provider,
                                  data_provider   = data_provider,
                                  declared_as     = attr_name)
        setattr(self, attr_name, relation)

    return self

  def __init__(self, backend):
    self.backend = backend
