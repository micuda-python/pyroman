from pyroman.core import expressions

class Column:
  def __init__(self, field):
    self._field = field

  def column_name(self, schema_provider):
    return schema_provider.column_name(field = self._field)

  def __lshift__(self, value):
    return expressions.Assignment(self, value)
