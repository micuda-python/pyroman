from abc import ABC, abstractmethod

class Expression(ABC):
  def __init__(self, column):
    self._column = column

  evaluate = abstractmethod(lambda self, schema_provider: None)

class Assignment(Expression):
  def __init__(self, column, value):
    super().__init__(column)
    self._value = value

  def evaluate(self, schema_provider):
    return (self._column.column_name(schema_provider), self._value)
