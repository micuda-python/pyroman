from pydantic import BaseModel, EmailStr

from pyroman                    import Relation, Schema
from pyroman.schema.constraints import max_length
from pyroman.relationships      import OneToMany

# setup
class User(BaseModel):
  id:       int
  username: str
  email:    EmailStr
  password: str

# Schema definition
## create schema by inheriting Schema
class UsersSchema(Schema):
  id       = fields.IntegerField()
  username = fields.CharField(max_length  = 100)
  email    = fields.EmailField(max_length = 100)
  password = fields.CharField(max_length  = 100)

## create schema from pydantic model
UsersSchema = Schema.from_pydantic_model(User)

# Relation helpers for creation of schema
## explicit assignment of Schema class
class Users(Relation):
  _schema_ = UsersSchema

## declarative way
class Users(Relation):
  class _schema_:
    id       = fields.IntegerField()
    username = fields.CharField(max_length  = 100)
    email    = fields.EmailField(max_length = 100)
    password = fields.CharField(max_length  = 100)

## implicitly from pydantic model
class Users(Relation):
  _schema_ = User

# Adding/overriding field properties
from pyroman                    import fields
from pyroman.schema.constraints import max_length

max_length_100 = max_length == 100

UsersSchema = Schema.from_pydantic_model(
  User,
  # adds/overrides constraints
  username = fields.Field(constraints = [max_length_100]),
  # overrides type of field
  password = fields.IntegerField(),
)

