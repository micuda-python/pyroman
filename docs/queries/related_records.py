from pydantic import BaseModel, EmailStr

from pyroman               import Relation, Repository, Schema
from pyroman.relationships import OneToMany

# setup
class User(BaseModel):
  id:       int
  username: str
  email:    EmailStr
  password: str

class Address(BaseModel):
  id:          int
  user_id:     int
  street:      str
  city:        str
  postal_code: str


Users     = Relation.for_schema(Schema.from_pydantic_model(User),
                                relationships = [HasMany(Addresses)])
Addresses = Relation.for_schema(Schema.from_pydantic_model(Address))
UsersRepo = Repository.for_relations(Users, Addresses)

# examples
users_repo = UsersRepo.in_memory()

users_with_addresses = \
  users_repo \
  .users \
  .with_(Users.r.addresses) \
  .where(Users.c.email.endswith('gmail.com')) \

# print user with related addresses
for user in users_with_addresses:
  print(user)

  for address in user.associations.addresses:
    print(address)

# print only addresses
for address in users_with_addresses.secondary_for(Users.r.addresses):
  print(address)
