from pydantic import BaseModel, Field

from pyroman import Schema, Relation, Index, mappers, fields, constraints, relationships

# notes
# * if the schema contains field named `id` that field is automatically considered as primary key
# * should I force setting primary key?
# * * I think I shouldn't.

class Addresses(Relation):
  pass
  # __schema__ = Address

class Users(Relation):
  # string that determines the name of table
  # TODO consider name `db_table`
  _table_name_ = None

  class _schema_:
    id       = fields.IntegerField()
    username = fields.CharField(max_length  = 100)
    email    = fields.EmailField(max_length = 100)
    password = fields.CharField(max_length  = 100)

  class _mappers_: # registered mappers
    pass

  _relationships_ = [relationships.HasMany(Addresses)]
  # or
  class _relationships_:
    addresses = relationships.HasMany(Addresses)

    # belongs_to:       (many-to-one)
    # has_many:         (one-to-many)
    # has_many-through: (many-to-many) via option `through`
    # has_one:          (one-to-one)
    # has_one-through:  (one-to-one-through)

  class _constraints_:
    primary_key = _schema_.id # can be iterable
    unique      = _schema_.email
    # technically FK is constraint too
    # constraints from https://www.w3schools.com/sql/sql_constraints.asp:
    # * not null
    # * unique
    # * primary key
    # * foreign key
    # * check
    # * default

  # class _indexes_:
  #   email = Index(_schema_.email)
  #   # way how to create unique index without creating constraint
  #   email = Index(_schema_.email, unique = True)

  class _config_:
    # TODO consider rename `attr` to `accessor`/`property`
    columns_property       = 'c'
    mappers_property       = 'm'
    relationships_property = 'r'
    # TODO consider name `mappers`
    default_mappers    = [mappers.Identity] # [mappers.Entity] will be default in future
    # e.g. ~_schema_.first_name for desc ordering by first_name, can be iterable
    # TODO consider name `ordering`
    # TODO maybe remove, user can override `create_dataset`
    default_ordering   = None
    # generates filter method for specified fields. Method name is equal to f`for_{field_name}`
    filter_method_for  = None # e.g. for value {_schema_.id} is generated method `for_id(id)`

  def by_email(self, *, email) -> User:
    return self.where(self.c.email == email).first()


# alt. primary_key declaration
class Users(Relation):
  class _schema_:
    id       = fields.IntegerField(primary_key = True)
    password = fields.CharField()
